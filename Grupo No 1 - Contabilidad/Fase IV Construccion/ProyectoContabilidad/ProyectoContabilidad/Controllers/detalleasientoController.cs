﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProyectoContabilidad.Models;

namespace ProyectoContabilidad.Controllers
{
    public class detalleasientoController : Controller
    {
        private contabilidadEntities2 db = new contabilidadEntities2();

        //
        // GET: /detalleasiento/

        public ActionResult Index()
        {
            var detalle_asiento = db.Detalle_asiento.Include("Asiento").Include("Cuenta").Include("Periodo1");
            return View(detalle_asiento.ToList());
        }

        //
        // GET: /detalleasiento/Details/5

        public ActionResult Details(string id = null)
        {
            Detalle_asiento detalle_asiento = db.Detalle_asiento.Single(d => d.num_asiento == id);
            if (detalle_asiento == null)
            {
                return HttpNotFound();
            }
            return View(detalle_asiento);
        }

        //
        // GET: /detalleasiento/Create

        public ActionResult Create()
        {
            ViewBag.num_asiento = new SelectList(db.Asiento, "num_asiento", "cod_periodo");
            ViewBag.num_cuenta = new SelectList(db.Cuenta, "num_cuenta", "nombre");
            ViewBag.periodo = new SelectList(db.Periodo, "cod_periodo", "cod_periodo");
            return View();
        }

        //
        // POST: /detalleasiento/Create

        [HttpPost]
        public ActionResult Create(Detalle_asiento detalle_asiento)
        {
            if (ModelState.IsValid)
            {
                db.Detalle_asiento.AddObject(detalle_asiento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.num_asiento = new SelectList(db.Asiento, "num_asiento", "cod_periodo", detalle_asiento.num_asiento);
            ViewBag.num_cuenta = new SelectList(db.Cuenta, "num_cuenta", "nombre", detalle_asiento.num_cuenta);
            ViewBag.periodo = new SelectList(db.Periodo, "cod_periodo", "cod_periodo", detalle_asiento.periodo);
            return View(detalle_asiento);
        }

        //
        // GET: /detalleasiento/Edit/5

        public ActionResult Edit(string id = null)
        {
            Detalle_asiento detalle_asiento = db.Detalle_asiento.Single(d => d.num_asiento == id);
            if (detalle_asiento == null)
            {
                return HttpNotFound();
            }
            ViewBag.num_asiento = new SelectList(db.Asiento, "num_asiento", "cod_periodo", detalle_asiento.num_asiento);
            ViewBag.num_cuenta = new SelectList(db.Cuenta, "num_cuenta", "nombre", detalle_asiento.num_cuenta);
            ViewBag.periodo = new SelectList(db.Periodo, "cod_periodo", "cod_periodo", detalle_asiento.periodo);
            return View(detalle_asiento);
        }

        //
        // POST: /detalleasiento/Edit/5

        [HttpPost]
        public ActionResult Edit(Detalle_asiento detalle_asiento)
        {
            if (ModelState.IsValid)
            {
                db.Detalle_asiento.Attach(detalle_asiento);
                db.ObjectStateManager.ChangeObjectState(detalle_asiento, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.num_asiento = new SelectList(db.Asiento, "num_asiento", "cod_periodo", detalle_asiento.num_asiento);
            ViewBag.num_cuenta = new SelectList(db.Cuenta, "num_cuenta", "nombre", detalle_asiento.num_cuenta);
            ViewBag.periodo = new SelectList(db.Periodo, "cod_periodo", "cod_periodo", detalle_asiento.periodo);
            return View(detalle_asiento);
        }

        //
        // GET: /detalleasiento/Delete/5

        public ActionResult Delete(string id = null)
        {
            Detalle_asiento detalle_asiento = db.Detalle_asiento.Single(d => d.num_asiento == id);
            if (detalle_asiento == null)
            {
                return HttpNotFound();
            }
            return View(detalle_asiento);
        }

        //
        // POST: /detalleasiento/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            Detalle_asiento detalle_asiento = db.Detalle_asiento.Single(d => d.num_asiento == id);
            db.Detalle_asiento.DeleteObject(detalle_asiento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}