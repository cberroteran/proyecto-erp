﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProyectoContabilidad.Models;

namespace ProyectoContabilidad.Controllers
{
    public class tipocuentaController : Controller
    {
        private contabilidadEntities2 db = new contabilidadEntities2();

        //
        // GET: /tipocuenta/

        public ActionResult Index()
        {
            return View(db.TipoCuenta.ToList());
        }

        //
        // GET: /tipocuenta/Details/5

        public ActionResult Details(int id = 0)
        {
            TipoCuenta tipocuenta = db.TipoCuenta.Single(t => t.num == id);
            if (tipocuenta == null)
            {
                return HttpNotFound();
            }
            return View(tipocuenta);
        }

        //
        // GET: /tipocuenta/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /tipocuenta/Create

        [HttpPost]
        public ActionResult Create(TipoCuenta tipocuenta)
        {
            if (ModelState.IsValid)
            {
                db.TipoCuenta.AddObject(tipocuenta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipocuenta);
        }

        //
        // GET: /tipocuenta/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TipoCuenta tipocuenta = db.TipoCuenta.Single(t => t.num == id);
            if (tipocuenta == null)
            {
                return HttpNotFound();
            }
            return View(tipocuenta);
        }

        //
        // POST: /tipocuenta/Edit/5

        [HttpPost]
        public ActionResult Edit(TipoCuenta tipocuenta)
        {
            if (ModelState.IsValid)
            {
                db.TipoCuenta.Attach(tipocuenta);
                db.ObjectStateManager.ChangeObjectState(tipocuenta, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipocuenta);
        }

        //
        // GET: /tipocuenta/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TipoCuenta tipocuenta = db.TipoCuenta.Single(t => t.num == id);
            if (tipocuenta == null)
            {
                return HttpNotFound();
            }
            return View(tipocuenta);
        }

        //
        // POST: /tipocuenta/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoCuenta tipocuenta = db.TipoCuenta.Single(t => t.num == id);
            db.TipoCuenta.DeleteObject(tipocuenta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}