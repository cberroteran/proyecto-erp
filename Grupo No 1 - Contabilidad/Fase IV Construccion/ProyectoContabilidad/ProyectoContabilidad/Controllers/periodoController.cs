﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProyectoContabilidad.Models;

namespace ProyectoContabilidad.Controllers
{
    public class periodoController : Controller
    {
        private contabilidadEntities2 db = new contabilidadEntities2();

        //
        // GET: /periodo/

        public ActionResult Index()
        {
            return View(db.Periodo.ToList());
        }

        //
        // GET: /periodo/Details/5

        public ActionResult Details(string id = null)
        {
            Periodo periodo = db.Periodo.Single(p => p.cod_periodo == id);
            if (periodo == null)
            {
                return HttpNotFound();
            }
            return View(periodo);
        }

        //
        // GET: /periodo/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /periodo/Create

        [HttpPost]
        public ActionResult Create(Periodo periodo)
        {
            if (ModelState.IsValid)
            {
                db.Periodo.AddObject(periodo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(periodo);
        }

        //
        // GET: /periodo/Edit/5

        public ActionResult Edit(string id = null)
        {
            Periodo periodo = db.Periodo.Single(p => p.cod_periodo == id);
            if (periodo == null)
            {
                return HttpNotFound();
            }
            return View(periodo);
        }

        //
        // POST: /periodo/Edit/5

        [HttpPost]
        public ActionResult Edit(Periodo periodo)
        {
            if (ModelState.IsValid)
            {
                db.Periodo.Attach(periodo);
                db.ObjectStateManager.ChangeObjectState(periodo, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(periodo);
        }

        //
        // GET: /periodo/Delete/5

        public ActionResult Delete(string id = null)
        {
            Periodo periodo = db.Periodo.Single(p => p.cod_periodo == id);
            if (periodo == null)
            {
                return HttpNotFound();
            }
            return View(periodo);
        }

        //
        // POST: /periodo/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            Periodo periodo = db.Periodo.Single(p => p.cod_periodo == id);
            db.Periodo.DeleteObject(periodo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}