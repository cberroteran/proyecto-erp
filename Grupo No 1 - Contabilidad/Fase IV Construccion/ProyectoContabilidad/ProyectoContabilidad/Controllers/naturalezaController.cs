﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProyectoContabilidad.Models;

namespace ProyectoContabilidad.Controllers
{
    public class naturalezaController : Controller
    {
        private contabilidadEntities2 db = new contabilidadEntities2();

        //
        // GET: /naturaleza/

        public ActionResult Index()
        {
            return View(db.Naturaleza.ToList());
        }

        //
        // GET: /naturaleza/Details/5

        public ActionResult Details(int id = 0)
        {
            Naturaleza naturaleza = db.Naturaleza.Single(n => n.cod == id);
            if (naturaleza == null)
            {
                return HttpNotFound();
            }
            return View(naturaleza);
        }

        //
        // GET: /naturaleza/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /naturaleza/Create

        [HttpPost]
        public ActionResult Create(Naturaleza naturaleza)
        {
            if (ModelState.IsValid)
            {
                db.Naturaleza.AddObject(naturaleza);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(naturaleza);
        }

        //
        // GET: /naturaleza/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Naturaleza naturaleza = db.Naturaleza.Single(n => n.cod == id);
            if (naturaleza == null)
            {
                return HttpNotFound();
            }
            return View(naturaleza);
        }

        //
        // POST: /naturaleza/Edit/5

        [HttpPost]
        public ActionResult Edit(Naturaleza naturaleza)
        {
            if (ModelState.IsValid)
            {
                db.Naturaleza.Attach(naturaleza);
                db.ObjectStateManager.ChangeObjectState(naturaleza, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(naturaleza);
        }

        //
        // GET: /naturaleza/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Naturaleza naturaleza = db.Naturaleza.Single(n => n.cod == id);
            if (naturaleza == null)
            {
                return HttpNotFound();
            }
            return View(naturaleza);
        }

        //
        // POST: /naturaleza/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Naturaleza naturaleza = db.Naturaleza.Single(n => n.cod == id);
            db.Naturaleza.DeleteObject(naturaleza);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}