USE [Inventario]
GO
/****** Object:  User [saa]    Script Date: 05/01/2013 08:57:53 ******/
CREATE USER [saa] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[Categoria]    Script Date: 05/01/2013 08:58:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categoria](
	[Id_Categoria] [int] NOT NULL,
	[Nombre_Categoria] [nchar](50) NOT NULL,
	[Descripcion] [nchar](100) NOT NULL,
 CONSTRAINT [PK_Categoria] PRIMARY KEY CLUSTERED 
(
	[Id_Categoria] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Inventario]    Script Date: 05/01/2013 08:58:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inventario](
	[Id_Bodega] [int] NOT NULL,
	[Id_Producto] [int] NOT NULL,
	[Stock_Min] [int] NOT NULL,
	[Stock_Max] [int] NOT NULL,
 CONSTRAINT [PK_Inventario] PRIMARY KEY CLUSTERED 
(
	[Id_Bodega] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Producto]    Script Date: 05/01/2013 08:58:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Producto](
	[Id_Producto] [int] NOT NULL,
	[Id_Categoria] [int] NOT NULL,
	[Nombre] [nchar](50) NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Precio] [int] NOT NULL,
 CONSTRAINT [PK_Producto] PRIMARY KEY CLUSTERED 
(
	[Id_Producto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Proveedor]    Script Date: 05/01/2013 08:58:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Proveedor](
	[Id_Proveedor] [int] NOT NULL,
	[Id_Producto] [int] NOT NULL,
	[Nombre] [nchar](50) NOT NULL,
	[Direccion] [nvarchar](50) NOT NULL,
	[Telefono] [int] NOT NULL,
	[Empresa] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Proveedor] PRIMARY KEY CLUSTERED 
(
	[Id_Proveedor] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Detalle_Producto]    Script Date: 05/01/2013 08:58:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Detalle_Producto](
	[Id_Producto] [int] NOT NULL,
	[Descripcion] [nchar](100) NOT NULL,
	[FechaCaducidad] [date] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Default [DF_Categoria_Id_Categoria]    Script Date: 05/01/2013 08:58:02 ******/
ALTER TABLE [dbo].[Categoria] ADD  CONSTRAINT [DF_Categoria_Id_Categoria]  DEFAULT ((4)) FOR [Id_Categoria]
GO
/****** Object:  Default [DF_Producto_Id_Producto]    Script Date: 05/01/2013 08:58:02 ******/
ALTER TABLE [dbo].[Producto] ADD  CONSTRAINT [DF_Producto_Id_Producto]  DEFAULT ((4)) FOR [Id_Producto]
GO
/****** Object:  Default [DF_Producto_Id_Categoria]    Script Date: 05/01/2013 08:58:02 ******/
ALTER TABLE [dbo].[Producto] ADD  CONSTRAINT [DF_Producto_Id_Categoria]  DEFAULT ((4)) FOR [Id_Categoria]
GO
/****** Object:  Default [DF_Proveedor_Id_Proveedor]    Script Date: 05/01/2013 08:58:02 ******/
ALTER TABLE [dbo].[Proveedor] ADD  CONSTRAINT [DF_Proveedor_Id_Proveedor]  DEFAULT ((4)) FOR [Id_Proveedor]
GO
/****** Object:  Default [DF_Proveedor_Id_Producto]    Script Date: 05/01/2013 08:58:02 ******/
ALTER TABLE [dbo].[Proveedor] ADD  CONSTRAINT [DF_Proveedor_Id_Producto]  DEFAULT ((4)) FOR [Id_Producto]
GO
/****** Object:  Default [DF_Proveedor_Telefono]    Script Date: 05/01/2013 08:58:02 ******/
ALTER TABLE [dbo].[Proveedor] ADD  CONSTRAINT [DF_Proveedor_Telefono]  DEFAULT ((8)) FOR [Telefono]
GO
/****** Object:  Default [DF_Detalle_Producto_Id_Producto]    Script Date: 05/01/2013 08:58:02 ******/
ALTER TABLE [dbo].[Detalle_Producto] ADD  CONSTRAINT [DF_Detalle_Producto_Id_Producto]  DEFAULT ((4)) FOR [Id_Producto]
GO
/****** Object:  ForeignKey [FK_Producto_Categoria]    Script Date: 05/01/2013 08:58:02 ******/
ALTER TABLE [dbo].[Producto]  WITH CHECK ADD  CONSTRAINT [FK_Producto_Categoria] FOREIGN KEY([Id_Categoria])
REFERENCES [dbo].[Categoria] ([Id_Categoria])
GO
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_Categoria]
GO
/****** Object:  ForeignKey [FK_Proveedor_Producto]    Script Date: 05/01/2013 08:58:02 ******/
ALTER TABLE [dbo].[Proveedor]  WITH CHECK ADD  CONSTRAINT [FK_Proveedor_Producto] FOREIGN KEY([Id_Producto])
REFERENCES [dbo].[Producto] ([Id_Producto])
GO
ALTER TABLE [dbo].[Proveedor] CHECK CONSTRAINT [FK_Proveedor_Producto]
GO
/****** Object:  ForeignKey [FK_Detalle_Producto_Inventario]    Script Date: 05/01/2013 08:58:02 ******/
ALTER TABLE [dbo].[Detalle_Producto]  WITH CHECK ADD  CONSTRAINT [FK_Detalle_Producto_Inventario] FOREIGN KEY([Id_Producto])
REFERENCES [dbo].[Inventario] ([Id_Bodega])
GO
ALTER TABLE [dbo].[Detalle_Producto] CHECK CONSTRAINT [FK_Detalle_Producto_Inventario]
GO
/****** Object:  ForeignKey [FK_Detalle_Producto_Producto]    Script Date: 05/01/2013 08:58:02 ******/
ALTER TABLE [dbo].[Detalle_Producto]  WITH CHECK ADD  CONSTRAINT [FK_Detalle_Producto_Producto] FOREIGN KEY([Id_Producto])
REFERENCES [dbo].[Producto] ([Id_Producto])
GO
ALTER TABLE [dbo].[Detalle_Producto] CHECK CONSTRAINT [FK_Detalle_Producto_Producto]
GO
