﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Inicio
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    ​<div class="hero-unit">
        <h1>
            Módulo de compras y adquisiciones</h1>
        <p>
            Mediante este módulo puede apoyar su proceso de compras y adquisiciones de productos
            a través de una automatización de la generación de cotizaciones, ordenes de compra
            y facturas de los proveedores. Con las funciones que le ofrece este sitio web y
            las credenciales adecuadas podrá desempeñar, de forma menos suceptible a errores,
            sus actividades.</p>
        <p>
            <a class="btn btn-primary btn-large" href="/Account/LogOn">Iniciar sesión »</a></p>
    </div>
    <div class="row">
        <div class="span4">
            <h2>
                Empleados</h2>
            <p>
                Los empleados que se encuentran en las distintas áreas de la empresa se encargan
                de registrar solicitudes de compra a través del sitio web especificando las cantidaded
                de que productos son requeridos para cubrir sus necesidades, en pro de mejorar la
                productividad de la empresa manteniendo un registro preciso de sus solicitudes y
                comprobaciones de que recibe lo que solicita.
            </p>
        </div>
        <div class="span4">
            <h2>
                Gerencia</h2>
            <p>
                Encargada de velar por el bienestar financiero y el clima organizacional de la institución
                que administra, la gerencia necesita poder visualizar rapidamente las solicitudes
                de compra que realizan sus empleados y aprobar o desaprobar en función del cumplimiento
                de los objetivos de la empresa. El sitio web suministra un simple interfaz para
                realizar dichas aprobaciones.
            </p>
        </div>
        <div class="span4">
            <h2>
                Compras</h2>
            <p>
                Como el miembro que realiza la mayoría de los trámites operativos para que los productos
                solicitados por los empleados lleguen a donde son necesitados, el departamenteo
                de compra necesita generar solicitudes de cotizaciones, registrar las cotizaciones
                que recibe de los proveedores y en base a la seleccionada poder generar una orden
                de compra para poder registrar posteriormente una factura cuando los productos llegan
                al alcance de los solicitantes.
            </p>
        </div>
    </div>
</asp:Content>
