﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ComprasMVC.Models.LogOnModel>" %>

<asp:Content ID="loginTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Iniciar sesión
</asp:Content>
<asp:Content ID="loginContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="form-signin">
        <h2 class="form-signin-heading">
            Iniciar sesión</h2>
        <p>
            Por favor ingrese su usuario y contraseña.
            <%: Html.ActionLink("Regístrese", "Register")%>
            si no tiene una cuenta.
        </p>
        <% using (Html.BeginForm())
           { %>
        <%: Html.ValidationSummary(true, "El inicio de sesión no tuvó éxito. Por favor corrija los errores e intente nuevamente.")%>
        <div>
            <div class="editor-label">
                <%: Html.LabelFor(m => m.UserName)%>
            </div>
            <div class="editor-field">
                <%: Html.TextBoxFor(m => m.UserName, new { placeholder = "Usuario", @class = "input-block-level" })%>
                <%: Html.ValidationMessageFor(m => m.UserName)%>
            </div>
            <div class="editor-label">
                <%: Html.LabelFor(m => m.Password)%>
            </div>
            <div class="editor-field">
                <%: Html.PasswordFor(m => m.Password, new { placeholder = "Contraseña",  @class = "input-block-level" })%>
                <%: Html.ValidationMessageFor(m => m.Password)%>
            </div>
            <div class="editor-label">
                <label for="rememberMe" class="checkbox">
                    No cerrar sesión<%: Html.CheckBoxFor(m => m.RememberMe)%></label>
            </div>
            <p>
                <input class="btn btn-large btn-primary" type="submit" value="Iniciar sesión" />
            </p>
        </div>
    </div>
    <% } %>
</asp:Content>
