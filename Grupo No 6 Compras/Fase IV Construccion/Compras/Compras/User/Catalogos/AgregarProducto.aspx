﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AgregarProducto.aspx.cs" Inherits="Compras.User.Catálogos.AgregarProducto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Agregar Producto</h2>
    <asp:EntityDataSource ID="ProductoEntityDataSource" runat="server"
        ContextTypeName="Compras.DAL.ComprasEntities" EnableFlattening="False"
        EnableInsert="True" EntitySetName="Producto">
    </asp:EntityDataSource>
    <asp:DetailsView ID="ProductoDetailsView" runat="server" 
        DataSourceID="ProductoEntityDataSource" AutoGenerateRows="False"
        DefaultMode="Insert" CssClass="table">
        <Fields>
            <asp:BoundField DataField="idProducto" HeaderText="idProducto" 
                SortExpression="idProducto" />
            <asp:BoundField DataField="descripcion" HeaderText="descripcion" 
                SortExpression="descripcion" />
            <asp:BoundField DataField="unidadMedida" HeaderText="unidadMedida" 
                SortExpression="unidadMedida" />
            <asp:BoundField DataField="abreviaturaUnidadMedida" HeaderText="abreviaturaUnidadMedida" 
                SortExpression="abreviaturaUnidadMedida" />
             <asp:CommandField ShowInsertButton="True" />
       </Fields>
    </asp:DetailsView>

</asp:Content>
