﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListaDeProductos.aspx.cs" Inherits="Compras.User.Catalogos.ListaDeProductos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Lista de Productos</h2>
<br />
    <asp:EntityDataSource ID="ProductoDataSource" runat="server" 
        ContextTypeName="Compras.DAL.ComprasEntities" 
        EnableDelete="True" EnableFlattening="False" EnableUpdate="True" 
        EntitySetName="Producto" Where="it.anulado = FALSE">
    </asp:EntityDataSource>
    <asp:GridView ID="ProductoGridView" runat="server" AllowPaging="True" 
        AllowSorting="True" AutoGenerateColumns="False" 
        DataKeyNames="idProducto" DataSourceID="ProductoDataSource"
        OnRowCommand="ProductoGridView_RowCommand"
        CssClass="table table-striped table-bordered" >
        <Columns>
            <asp:CommandField ShowEditButton="True" />
            <asp:BoundField DataField="idProducto" HeaderText="ID Producto" 
                SortExpression="idProducto" />
            <asp:BoundField DataField="descripcion" HeaderText="Descripcion" 
                SortExpression="descripcion" />
            <asp:BoundField DataField="unidadMedida" HeaderText="Unidad Medida" 
                SortExpression="unidadMedida" />
            <asp:BoundField DataField="abreviaturaUnidadMedida" HeaderText="Abreviatura" 
                SortExpression="abreviaturaUnidadMedida" />
            <asp:ButtonField ButtonType="Link" CommandName="Anular" Text="Anular" />
        </Columns>
    </asp:GridView>
</asp:Content>
