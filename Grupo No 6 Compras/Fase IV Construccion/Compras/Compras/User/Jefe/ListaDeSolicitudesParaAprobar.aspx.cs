﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Compras.DAL;

namespace Compras.User.Jefe
{
    public partial class ListaDeSolicitudesParaAprobar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void SolicitudCompraGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            switch (e.CommandName)
            {
                case "Aprobar":
                    var id1 = Convert.ToInt64(SolicitudCompraGridView.Rows[Convert.ToInt32(e.CommandArgument)].Cells[1].Text);

                    using (var context = new ComprasEntities())
                    {
                        SolicitudCompra sc = context.SolicitudCompra.First(i => i.idSolicitudCompra == id1);
                        sc.aprobado = true;
                        context.SaveChanges();
                    }
                    break;

                case "Rechazar":
                    var id2 = Convert.ToInt64(SolicitudCompraGridView.Rows[Convert.ToInt32(e.CommandArgument)].Cells[1].Text);

                    using (var context = new ComprasEntities())
                    {
                        SolicitudCompra sc = context.SolicitudCompra.First(i => i.idSolicitudCompra == id2);
                        sc.aprobado = false;
                        context.SaveChanges();
                    }
                    break;
            }

            SolicitudCompraGridView.DataBind();

        }

    }
}