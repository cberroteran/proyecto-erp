﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CuentasPorPagar.Models;

namespace CuentasPorPagar.Controllers
{
    public class FormaPagoController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /FormaPago/

        public ActionResult Index()
        {
            return View(db.FormaDePagoes.ToList());
        }

        //
        // GET: /FormaPago/Details/5

        public ActionResult Details(int id = 0)
        {
            FormaDePago formadepago = db.FormaDePagoes.Find(id);
            if (formadepago == null)
            {
                return HttpNotFound();
            }
            return View(formadepago);
        }

        //
        // GET: /FormaPago/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /FormaPago/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormaDePago formadepago)
        {
            if (ModelState.IsValid)
            {
                db.FormaDePagoes.Add(formadepago);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(formadepago);
        }

        //
        // GET: /FormaPago/Edit/5

        public ActionResult Edit(int id = 0)
        {
            FormaDePago formadepago = db.FormaDePagoes.Find(id);
            if (formadepago == null)
            {
                return HttpNotFound();
            }
            return View(formadepago);
        }

        //
        // POST: /FormaPago/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FormaDePago formadepago)
        {
            if (ModelState.IsValid)
            {
                db.Entry(formadepago).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(formadepago);
        }

        //
        // GET: /FormaPago/Delete/5

        public ActionResult Delete(int id = 0)
        {
            FormaDePago formadepago = db.FormaDePagoes.Find(id);
            if (formadepago == null)
            {
                return HttpNotFound();
            }
            return View(formadepago);
        }

        //
        // POST: /FormaPago/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FormaDePago formadepago = db.FormaDePagoes.Find(id);
            db.FormaDePagoes.Remove(formadepago);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}