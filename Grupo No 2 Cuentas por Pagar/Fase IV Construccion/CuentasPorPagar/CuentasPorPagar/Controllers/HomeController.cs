﻿using CuentasPorPagar.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CuentasPorPagar.Controllers
{
    
    public class HomeController : Controller
    {
        private CuentasPorPagarEntities db = new CuentasPorPagarEntities();
        [Authorize(Roles = "Gerente, Auxiliar")]
        public ActionResult Index()
        {
            return View(db.Proveedors.ToList());
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Página de contacto.";

            return View();
        }

        public ActionResult Detalles(int id) 
        {
            double saldo=0.0;
            var deudas = from x in db.Deudas
                        where x.idProveedor == id
                        select x;
            List<Historico> movimientos = new List<Historico>();

            foreach (var deuda in deudas)
            {
                movimientos.Add(new Historico{Detalle=deuda.Detalle,Fecha=(DateTime)deuda.fechaInicio,Debe=0.0F,Haber=(float)deuda.cantidadDeuda});
                foreach(var mov in deuda.DetalleDePagoes){
                    
                    movimientos.Add(new Historico{Detalle=mov.Pago.detalle,Fecha=(DateTime)mov.fecha,Debe=(float)mov.monto,Haber=0.0F});
                }
                
            }

            foreach (var mov in movimientos.OrderBy(x => x.Fecha))
            {
                if (mov.Debe == 0.0F)
                {
                    saldo = saldo + (double)mov.Haber;
                    mov.Saldo = (float)saldo;
                }
                else
                {
                    saldo = saldo - (double)mov.Debe;
                    mov.Saldo =(float)saldo;
                }
                
            }
            return View(movimientos);
        }
    }
}
