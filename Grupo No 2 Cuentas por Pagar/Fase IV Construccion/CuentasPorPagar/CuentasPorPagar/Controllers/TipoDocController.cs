﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CuentasPorPagar.Models;

namespace CuentasPorPagar.Controllers
{
    public class TipoDocController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /TipoDoc/

        public ActionResult Index()
        {
            return View(db.tipoDocumentoes.ToList());
        }

        //
        // GET: /TipoDoc/Details/5

        public ActionResult Details(int id = 0)
        {
            tipoDocumento tipodocumento = db.tipoDocumentoes.Find(id);
            if (tipodocumento == null)
            {
                return HttpNotFound();
            }
            return View(tipodocumento);
        }

        //
        // GET: /TipoDoc/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /TipoDoc/Create

        [HttpPost]
        public ActionResult Create(tipoDocumento tipodocumento)
        {
            if (ModelState.IsValid)
            {
                db.tipoDocumentoes.Add(tipodocumento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipodocumento);
        }

        //
        // GET: /TipoDoc/Edit/5

        public ActionResult Edit(int id = 0)
        {
            tipoDocumento tipodocumento = db.tipoDocumentoes.Find(id);
            if (tipodocumento == null)
            {
                return HttpNotFound();
            }
            return View(tipodocumento);
        }

        //
        // POST: /TipoDoc/Edit/5

        [HttpPost]
        public ActionResult Edit(tipoDocumento tipodocumento)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipodocumento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipodocumento);
        }

        //
        // GET: /TipoDoc/Delete/5

        public ActionResult Delete(int id = 0)
        {
            tipoDocumento tipodocumento = db.tipoDocumentoes.Find(id);
            if (tipodocumento == null)
            {
                return HttpNotFound();
            }
            return View(tipodocumento);
        }

        //
        // POST: /TipoDoc/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            tipoDocumento tipodocumento = db.tipoDocumentoes.Find(id);
            db.tipoDocumentoes.Remove(tipodocumento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}