﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CuentasPorPagar.Models;

namespace CuentasPorPagar.Controllers
{
    public class DeudaController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Deuda/

        public ActionResult Index()
        {
            var deudas = db.Deudas.Include(d => d.Documento).Include(d => d.Proveedor);
            return View(deudas.ToList());
        }

        //
        // GET: /Deuda/Details/5

        public ActionResult Details(int id = 0)
        {
            Deuda deuda = db.Deudas.Find(id);
            if (deuda == null)
            {
                return HttpNotFound();
            }
            return View(deuda);
        }

        //
        // GET: /Deuda/Create

        public ActionResult Create()
        {
            ViewBag.tipoDocs = new SelectList(db.tipoDocumentoes, "idTipoDocumento", "nombre");
            ViewBag.idProveedor = new SelectList(db.Proveedors, "idProveedor", "Nombre");      
            Deuda deuda=new Deuda();
            Documento doc = new Documento();       
            deuda.Documento=doc;
            deuda.fechaInicio = DateTime.Now;
            deuda.fechaFin = DateTime.Now;
            return View(deuda);
        }

        //
        // POST: /Deuda/Create

        [HttpPost]
        public ActionResult Create(Deuda deuda)
        {
            if (ModelState.IsValid)
            {
                
                string physicalPath = HttpContext.Server.MapPath("../") + "UploadImages" + "\\";

                for (int i = 0; i < Request.Files.Count; i++)
                {
                    String urlDoc = physicalPath + System.IO.Path.GetFileName(Request.Files[i].FileName);
                    Request.Files[0].SaveAs(urlDoc);
                    deuda.Documento.URL = urlDoc;
                }
                tipoDocumento tipoDoc = db.tipoDocumentoes.Find(deuda.Documento.idTipoDocumento);
                deuda.Documento.tipoDocumento = tipoDoc;
                db.Documentoes.Add(deuda.Documento);
                deuda.Documento.URL = "url";
                db.SaveChanges();
                db.Deudas.Add(deuda);
                db.SaveChanges();

                return RedirectToAction("Index","Home");
            }

            ViewBag.idDocumento = new SelectList(db.Documentoes, "idDocumento", "URL", deuda.idDocumento);
            ViewBag.idProveedor = new SelectList(db.Proveedors, "idProveedor", "Direccion", deuda.idProveedor);
            return View(deuda);
        }

        //
        // GET: /Deuda/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Deuda deuda = db.Deudas.Find(id);
            if (deuda == null)
            {
                return HttpNotFound();
            }
            ViewBag.idDocumento = new SelectList(db.Documentoes, "idDocumento", "URL", deuda.idDocumento);
            ViewBag.idProveedor = new SelectList(db.Proveedors, "idProveedor", "Direccion", deuda.idProveedor);
            return View(deuda);
        }

        //
        // POST: /Deuda/Edit/5

        [HttpPost]
        public ActionResult Edit(Deuda deuda)
        {
            if (ModelState.IsValid)
            {
                db.Entry(deuda).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idDocumento = new SelectList(db.Documentoes, "idDocumento", "URL", deuda.idDocumento);
            ViewBag.idProveedor = new SelectList(db.Proveedors, "idProveedor", "Direccion", deuda.idProveedor);
            return View(deuda);
        }

        //
        // GET: /Deuda/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Deuda deuda = db.Deudas.Find(id);
            if (deuda == null)
            {
                return HttpNotFound();
            }
            return View(deuda);
        }

        //
        // POST: /Deuda/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Deuda deuda = db.Deudas.Find(id);
            db.Deudas.Remove(deuda);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}