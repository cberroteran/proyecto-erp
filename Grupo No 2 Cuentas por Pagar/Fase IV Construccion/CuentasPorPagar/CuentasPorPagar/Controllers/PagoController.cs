﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CuentasPorPagar.Models;

namespace CuentasPorPagar.Controllers
{
    public class PagoController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Pago/

        public ActionResult Index()
        {
            var pagoes = db.Pagoes.Include(p => p.FormaDePago);
            return View(pagoes.ToList());
        }

        //
        // GET: /Pago/Details/5

        public ActionResult Details(int id = 0)
        {
            Pago pago = db.Pagoes.Find(id);
            if (pago == null)
            {
                return HttpNotFound();
            }
            return View(pago);
        }


        //public ActionResult SeleccionProveedor() {
        //    ViewBag.Proveedores = new SelectList(db.Proveedors, "idProveedor", "Nombre");
        //    return View();
        //}


        //[HttpPost]
        //public ActionResult SeleccionProveedor()
        //{
            
        //    return View();
        //}

        //
        // GET: /Pago/Create

        public ActionResult Create(String idProveedor)
        {
            ViewBag.idFormaDePago = new SelectList(db.FormaDePagoes, "idFormaDePago", "descripcion");
            //var listaDeudas= from x in db.Deudas where x.idProveedor=Convert.ToInt32(idProveedor);
            ViewBag.deudas = new SelectList(db.Proveedors, "idProveedor", "Nombre");
            return View();
        }

        //
        // POST: /Pago/Create



        [HttpPost]
        public ActionResult Create(Pago pago)
        {
            if (ModelState.IsValid)
            {
                db.Pagoes.Add(pago);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idFormaDePago = new SelectList(db.FormaDePagoes, "idFormaDePago", "descripcion", pago.idFormaDePago);
            return View(pago);
        }

        //
        // GET: /Pago/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Pago pago = db.Pagoes.Find(id);
            if (pago == null)
            {
                return HttpNotFound();
            }
            ViewBag.idFormaDePago = new SelectList(db.FormaDePagoes, "idFormaDePago", "descripcion", pago.idFormaDePago);
            return View(pago);
        }

        //
        // POST: /Pago/Edit/5

        [HttpPost]
        public ActionResult Edit(Pago pago)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pago).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idFormaDePago = new SelectList(db.FormaDePagoes, "idFormaDePago", "descripcion", pago.idFormaDePago);
            return View(pago);
        }

        //
        // GET: /Pago/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Pago pago = db.Pagoes.Find(id);
            if (pago == null)
            {
                return HttpNotFound();
            }
            return View(pago);
        }

        //
        // POST: /Pago/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Pago pago = db.Pagoes.Find(id);
            db.Pagoes.Remove(pago);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}